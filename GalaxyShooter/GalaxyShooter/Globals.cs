﻿using GalaxyShooter.JSONInterpreter;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter
{
    public class Globals

    {
        private static Globals instance;
        public int ScreenWidth { get; set; }
        public int ScreenHeight { get; set; }
        public SpriteBatch spriteBatch { get; set; }
        public GameContent GameContent { get; set; }
        public Boolean LoadCustomBehaviors { get; set; }
        public List<CustomBehavior> LoadedBehaviors { get; set; }

        public Globals()
        {
            ScreenWidth = 0;
            ScreenHeight = 0;
            LoadCustomBehaviors = false;
            LoadedBehaviors = new List<CustomBehavior>();

        }

        public static Globals Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Globals();                    
                }

                return instance;
            }
        }
        public static void PlaySound(SoundEffect sound)
        {
            float volume = 1;
            float pitch = 0.0f;
            float pan = 0.0f;
            sound.Play(volume, pitch, pan);
        }
    }
}
