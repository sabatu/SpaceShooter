﻿using GalaxyShooter.Models;
using GalaxyShooter.Models.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace GalaxyShooter.Interfaces
{
    interface BaseController
    {
        List<Ship> ReturnModelInstances();
        void FireWeapons();
        List<Projectile> ReturnProjectilesToRender();
    }
}

