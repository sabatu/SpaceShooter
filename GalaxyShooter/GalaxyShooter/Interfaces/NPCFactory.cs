﻿using GalaxyShooter.Models;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter
{
    interface NPCFactory
    {
        List<Ship> GetRegularEnemy(string type, int SizeOfGroup);
        Ship GetBoss(string type);
    }
}
