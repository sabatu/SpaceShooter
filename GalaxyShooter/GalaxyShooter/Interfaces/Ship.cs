﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Interfaces
{
    interface BaseShip
    {
        void Draw();
        void Move();
        void FireWeapon(bool withSound);     
    }
}
