﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.JSONInterpreter
{
    public class JSONReader
    {
        public JSONReader() {}

        public List<CustomBehavior> ReadInEnemyBehaviors()
        {
            List<CustomBehavior> EnemyBehaviors = new List<CustomBehavior>();

            using (StreamReader reader = new StreamReader("C:\\users\\sabatu\\desktop\\Level1.json"))
            {
                string ReadInBehavior = reader.ReadToEnd();
                EnemyBehaviors.AddRange(JsonConvert.DeserializeObject<List<CustomBehavior>>(ReadInBehavior));
            }

            return EnemyBehaviors;
        }


    }
}
