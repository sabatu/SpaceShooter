﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.JSONInterpreter
{
    public class CustomBehavior
    {
        public CustomBehavior() { }

        public string EnemyName { get; set; }
        public int HorizontalSpacing { get; set; }
        public int VerticalSpacing { get; set; }
        public int XVelocityMinInterval { get; set; }
        public int XVelocityMaxInterval { get; set; }
        public int YVelocityMinInterval { get; set; }
        public int YVelocityMaxInterval { get; set; }
        public string LaserColor { get; set; }
        public float LaserVelocity { get; set; }
        public int EnemyHP { get; set; }
        public int DefaultWeaponDamage { get; set; }
    }
}
