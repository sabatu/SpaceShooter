﻿using GalaxyShooter.Interfaces;
using GalaxyShooter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Context
{
    public class EnemyCreationContext
    {
        private EnemyCreationStrategy strategy;

        public EnemyCreationContext(EnemyCreationStrategy strategy)
        {
            this.strategy = strategy;
        }

        public List<Ship> executeEnemyCreationStrategy(string type, int SizeOfGroup)
        {
            return strategy.GetRegularEnemy(type, SizeOfGroup);
        }

        public Ship executeBossCreationStrategy(string type)
        {
            return strategy.GetBoss(type);
        }
    }
}
