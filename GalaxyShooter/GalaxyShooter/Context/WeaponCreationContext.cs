﻿using GalaxyShooter.Models.Abstracts;
using GalaxyShooter.Strategies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Context
{
    public class WeaponCreationContext
    {
        private WeaponCreationStrategy strategy;

        public WeaponCreationContext(WeaponCreationStrategy strategy)
        {
            this.strategy = strategy;
        }

        public Weapon executePlayerWeaponCreationStrategy(string type)
        {
            return strategy.ReturnPlayerWeapon(type);
        }

        public Weapon executeEnemyWeaponCreationStrategy(string type)
        {
            return strategy.ReturnEnemyWeapon(type);
        }
    }
}
