﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GalaxyShooter
{
    class GameBackground
    {
        public float Width { get; set; } //width of game
        public float Height { get; set; } //height of game

        private Texture2D background { get; set; }  //cached image single pixel we'll use to draw the border lines
        private SpriteBatch spriteBatch;  //allows us to write on backbuffer when we need to draw self

        public GameBackground()
        {
            Width = Globals.Instance.ScreenWidth;
            Height = Globals.Instance.ScreenHeight;
            background = Globals.Instance.GameContent.SpaceBackground;
            this.spriteBatch = Globals.Instance.spriteBatch;
        }

        public void Draw()
        {
            spriteBatch.Draw(background, new Rectangle(0, 0, 800, 800), Color.White);  //canvas map with static image for now

        }
    }
}
