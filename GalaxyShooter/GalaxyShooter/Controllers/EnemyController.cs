﻿using GalaxyShooter.Interfaces;
using GalaxyShooter.JSONInterpreter;
using GalaxyShooter.Models;
using GalaxyShooter.Models.Abstracts;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Controllers
{
    class EnemyController : BaseController
    {        
        private EnemyClient theNPCClient;
        private List<Ship> currentEnemies;

        List<Projectile> ProjectilesToRender = new List<Projectile>();

        public EnemyController()
        {
            this.theNPCClient = new EnemyClient();  
            this.currentEnemies = new List<Ship>();
        }

        public void CreateExplicitEnemyWave(int GroupSize,
                                            String EnemyType)
        {
            if (Globals.Instance.LoadCustomBehaviors)
            {
                currentEnemies.AddRange(theNPCClient.ReturnCustomExplicitEnemyWave(                                       
                                        GroupSize,
                                        EnemyType));
            }
            else
            {
                currentEnemies.AddRange(theNPCClient.ReturnExplicitEnemyWave(                                        
                                        GroupSize,
                                        EnemyType));
            }
        }

        public void CreateRandomEnemyWave(NPCFactory theEnemyFactory,
                                                   int GroupSize)
        {
            if (Globals.Instance.LoadCustomBehaviors)
            {
                currentEnemies.AddRange(theNPCClient.ReturnCustomRandomEnemyWave(GroupSize));
            }
            else
            {
                currentEnemies.AddRange(theNPCClient.ReturnRandomEnemyWave(GroupSize));
            }
        }


        public void CreateBoss(NPCFactory theEnemyFactory,
                               string Type)
        {

            if (Globals.Instance.LoadCustomBehaviors)
            {
                currentEnemies.Add(theNPCClient.ReturnCustomBoss(Type));
            }
            else
            {
                currentEnemies.Add(theNPCClient.ReturnPrebuiltBoss(Type));
            }
        }



        public List<Ship> ReturnModelInstances()
        {
            return this.currentEnemies;
        }

        public void FireWeapons()
        {
            foreach(Ship Enemy in this.currentEnemies)
            {
                //Fire weapons without sound, for now.
                Enemy.FireWeapon(false);
            }
        }

        public List<Projectile> ReturnProjectilesToRender()
        {
            ProjectilesToRender = new List<Projectile>();

            foreach (Ship Enemy in this.currentEnemies)
            {
                foreach (Weapon ShipWeapon in Enemy.ShipWeapons)
                {
                    if (ShipWeapon.ReturnProjectilesToRender().Count > 0)
                    {
                        foreach (Projectile enemyProjectiles in ShipWeapon.ReturnProjectilesToRender())
                        {
                            if (enemyProjectiles.Visible == true)
                            {
                                ProjectilesToRender.Add(enemyProjectiles);
                            }

                        }

                    }
                }
            }

            return ProjectilesToRender;
        }

        public void LoadCustomEnemies()
        {
            JSONReader Interpreter = new JSONReader();
            List<CustomBehavior> BehaviorsToLoad = Interpreter.ReadInEnemyBehaviors();

            Globals.Instance.LoadCustomBehaviors = true;
            Globals.Instance.LoadedBehaviors = BehaviorsToLoad;
        }

    }
}
