﻿using GalaxyShooter.Models;
using GalaxyShooter.Models.Abstracts;
using GalaxyShooter.Observers;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Controllers
{
    public class WeaponController : PlayerScoreObserver
    {

        public List<Weapon> WeaponsToRender = new List<Weapon>();

        public void DetectCollisions(List<Ship> ActiveShips, List<Projectile> ActiveProjectiles)
        {

            foreach (Projectile proj in ActiveProjectiles)
            {
                //check for ship hit
                //ship is 70 pixels.
                Rectangle projectileRect = new Rectangle((int)proj.x,
                                                         (int)proj.y,
                                                         (int)proj.Width,
                                                         (int)proj.Height);

                foreach (Ship ship in ActiveShips)
                {
                    Rectangle r1 = new Rectangle((int)ship.X,
                                                 (int)ship.Y,
                                                 (int)ship.Width,
                                                 (int)ship.Height);

                    if (HitTest(r1, projectileRect))
                    {
                        Globals.PlaySound(Globals.Instance.GameContent.LongExplosion);

                        ship.ShipHP -= proj.ProjectileHPDamage;

                        if (ship.ShipHP <= 0)
                        {
                            ship.Dead = true;
                        }

                        proj.StruckObject = true;
                        break;
                    }
                }
            }
        }


        public void DetectWeaponCollisions(Ship ActiveShip, Weapon WeaponDrop)
        {
                //check for ship hit
                //ship is 70 pixels.
                Rectangle projectileRect = new Rectangle((int)WeaponDrop.X,
                                                         (int)WeaponDrop.Y,
                                                         (int)WeaponDrop.Width,
                                                         (int)WeaponDrop.Height);

                    Rectangle r1 = new Rectangle((int)ActiveShip.X,
                                                 (int)ActiveShip.Y,
                                                 (int)ActiveShip.Width,
                                                 (int)ActiveShip.Height);

                    if (HitTest(r1, projectileRect))
                    {
                        ActiveShip.AddWeapon(WeaponDrop);
                        WeaponDrop.OffScreen = true;
                    }
                
            
        }

        public override void DropWeaponPowerup()
        {
            WeaponClient WeaponConstructor = new WeaponClient();

            if (Globals.Instance.LoadCustomBehaviors)
            {
                WeaponsToRender.Add(WeaponConstructor.ReturnRandomPlayerCustomWeapon());
            }
            else
            {
                WeaponsToRender.Add(WeaponConstructor.ReturnRandomPlayerPrebuiltWeapon());
            }                
        }

        private static bool HitTest(Rectangle r1, Rectangle r2)
        {
            if (Rectangle.Intersect(r1, r2) != Rectangle.Empty)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Weapon> ReturnWeaponsToRender()
        {
            return this.WeaponsToRender;
        }
        



    }
}
