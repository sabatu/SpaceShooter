﻿using GalaxyShooter.Interfaces;
using GalaxyShooter.Models;
using GalaxyShooter.Models.Abstracts;
using GalaxyShooter.Observers;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Controllers
{
    public class PlayerController :  Interfaces.BaseController
    {
        private Ship playerShip;        

        // this will be false for normal speed, true for half speed
        public bool speedToggle = false;

        //Used to keep track of player score intervals for weapon drops.
        private int PlayerScoreBenchmark = 0;

        public PlayerController()
        {
        }

        public void CreatePlayer(WeaponController CurrentWeaponController)
        {
            
            int playerShipX = (Globals.Instance.ScreenWidth - Globals.Instance.GameContent.PlayerShip.Width) / 2; //we'll center the player ship on the screen to start
            int playerShipY = Globals.Instance.ScreenHeight - 100;  //ship will be 100 pixels from the bottom of the screen

            int VelocityOfPlayerLaserY = -20;
            int playerHP = 1;
            int DefaultWeaponsDamage = 30;

            playerShip = new Ship(playerShipX,
                                     playerShipY,
                                     0,
                                     0,
                                     Globals.Instance.GameContent.PlayerShip,
                                     "Red",
                                     VelocityOfPlayerLaserY,
                                     playerHP,
                                     DefaultWeaponsDamage);

            playerShip.attach(CurrentWeaponController);
            
        }

        public List<Ship> ReturnModelInstances()
        {
            return new List<Ship>() { playerShip };
        }

        public void MoveLeft()
        {
            // normal speed
            if (speedToggle == false)
            {
                playerShip.X = playerShip.X - 5;
                if (playerShip.X < 1)
                {
                    playerShip.X = 1;
                }
            }
            // half speed
            else
            {
                playerShip.X = playerShip.X - (float)2.5;
                if (playerShip.X < 1)
                {
                    playerShip.X = 1;
                }

            }
        }
        public void MoveRight()
        {
            // normal speed
            if (speedToggle == false)
            {
                playerShip.X = playerShip.X + 5;
                if ((playerShip.X + playerShip.Width) > Globals.Instance.ScreenWidth)
                {
                    playerShip.X = Globals.Instance.ScreenWidth - playerShip.Width;
                }
            }
            // half speed
            else
            {
                playerShip.X = playerShip.X + (float)2.5;
                if ((playerShip.X + playerShip.Width) > Globals.Instance.ScreenWidth)
                {
                    playerShip.X = Globals.Instance.ScreenWidth - playerShip.Width;
                }
            }
        }

        public void MoveUp()
        {
            // normal speed
            if (speedToggle == false)
            {
                playerShip.Y = playerShip.Y - 5;
                if (playerShip.Y < 1)
                {
                    playerShip.Y = 1;
                }
            }
            else
            {
                playerShip.Y = playerShip.Y - (float)2.5;
                if (playerShip.Y < 1)
                {
                    playerShip.Y = 1;
                }
            }
        }
        public void MoveDown()
        {
            // normal speed
            if (speedToggle == false)
            {
                playerShip.Y = playerShip.Y + 5;
                if ((playerShip.Y + playerShip.Width) > Globals.Instance.ScreenWidth)
                {
                    playerShip.Y = Globals.Instance.ScreenWidth - playerShip.Width;
                }
            }
            // reduced speed
            else
            {
                playerShip.Y = playerShip.Y + (float)2.5;
                if ((playerShip.Y + playerShip.Width) > Globals.Instance.ScreenWidth)
                {
                    playerShip.Y = Globals.Instance.ScreenWidth - playerShip.Width;
                }
            }
        }

        public void MoveTo(float x, float y)
        {
            if (x >= 0)
            {
                if (x < Globals.Instance.ScreenWidth - playerShip.Width)
                {
                    playerShip.X = x;
                }
                else
                {
                    playerShip.X = Globals.Instance.ScreenWidth - playerShip.Width;
                }
            }
            else
            {
                if (x < 0)
                {
                    playerShip.X = 0;
                }
            }

            if (y >= 0)
            {
                if (y < Globals.Instance.ScreenWidth - playerShip.Width)
                {
                    playerShip.Y = y;
                }
                else
                {
                    playerShip.Y = Globals.Instance.ScreenWidth - playerShip.Width;
                }
            }
            else
            {
                if (y < 0)
                {
                    playerShip.Y = 0;
                }
            }
        }

        public void playerSpeedChangeUp()
        {
            speedToggle = false;
        }
        public void playerSpeedChangeDown()
        {
            speedToggle = true;
        }

        public void ModifyPlayerScore(int ScoreModifier)
        {
            playerShip.Score += ScoreModifier;            
        }

        public int ReturnPlayerScore()
        {
            return playerShip.Score;
        }

        public void FireWeapons()
        {
            playerShip.FireWeapon(true);
        }

        public List<Projectile> ReturnProjectilesToRender()
        {
            List<Projectile> ProjToRender = new List<Projectile>();
            
            foreach(Weapon ShipWeapon in playerShip.ShipWeapons)
            {
                ProjToRender.AddRange(ShipWeapon.ReturnProjectilesToRender());
            }

            return ProjToRender;
        }

        public void TogglePlayerInvincibility()
        {
            if(playerShip.ShipHP == 1)
            {
                playerShip.ShipHP = 100000;
            }
            else if(playerShip.ShipHP < 100000 && playerShip.ShipHP > 1)
            {
                playerShip.ShipHP = 1;
            }
        }





    }
}
