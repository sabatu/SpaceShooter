﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaxyShooter.Models;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GalaxyShooter.Interfaces;
using GalaxyShooter.Models.Abstracts;

namespace GalaxyShooter
{
    public class LaserWeapon : Weapon
    {
        //Call base constructor for initializing weapon to a parent ship.
        public LaserWeapon(int Damage,
                           GetShipXCoords GetXCoords,
                           GetShipYCoords GetYCoords,
                           string ProjectileColor,
                           float YVelocityOfProjectile) :base(Damage,
                           GetXCoords,
                           GetYCoords,
                           ProjectileColor,
                           YVelocityOfProjectile)
        {

        }

        //Call base constructor for initializing weapon to dead space.

        public LaserWeapon(float x,
                           float y,
                           float xVelocity,
                           float yVelocity,
                           string ProjectileColor,
                           int Damage,
                           float YVelocityOfProjectile) : base(x,
                           y,
                           xVelocity,
                           yVelocity,
                           ProjectileColor,
                           Damage,
                           YVelocityOfProjectile)
        {
            this.weaponSprite = Globals.Instance.GameContent.LaserDrop;
        }


        private List<Projectile> LasersToRender = new List<Projectile>();

        public override void FireWeapon(Boolean withSound,
                                        int XDiff = 0)
        {
            Laser FiredLaser = new Laser(BaseDamage,
                                        getX() + XDiff,
                                        getY(),
                                         0,
                                         this.ProjectileYVel,
                                         this.ColorOfProjectile);

            if (withSound)
            {
                FiredLaser.MakeSound();
            }

            FiredLaser.Visible = true;

            LasersToRender.Add(FiredLaser);
        }

        public override List<Projectile> ReturnProjectilesToRender()
        {
            return LasersToRender;
        }
    }
}
