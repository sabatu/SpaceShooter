﻿using GalaxyShooter.Models.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Models.Derived.Weapons
{
    class MissileLauncher : Weapon
    {
        //Call base constructor for initializing weapon to a parent ship.
        public MissileLauncher(int Damage,
                           GetShipXCoords GetXCoords,
                           GetShipYCoords GetYCoords,
                           string ProjectileColor,
                           float YVelocityOfProjectile) :base(Damage,
                           GetXCoords,
                           GetYCoords,
                           ProjectileColor,
                           YVelocityOfProjectile)
        {

        }

        //Call base constructor for initializing weapon to dead space.

        public MissileLauncher(float x,
                           float y,
                           float xVelocity,
                           float yVelocity,
                           string ProjectileColor,
                           int Damage,
                           float YVelocityOfProjectile) : base(x,
                           y,
                           xVelocity,
                           yVelocity,
                           ProjectileColor,
                           Damage,
                           YVelocityOfProjectile)
        {
            this.weaponSprite = Globals.Instance.GameContent.MissileDrop;
        }

        private List<Projectile> MissilesToRender = new List<Projectile>();

        public override void FireWeapon(Boolean withSound,
                                        int XDiff = 0)
        {
            Missile FiredMissile = new Missile(BaseDamage,
                                        getX() + XDiff,
                                        getY(),
                                         0,
                                         this.ProjectileYVel,
                                         this.ColorOfProjectile);

            if (withSound)
            {
                FiredMissile.MakeSound();
            }

            FiredMissile.Visible = true;

            MissilesToRender.Add(FiredMissile);
        }

        public override List<Projectile> ReturnProjectilesToRender()
        {
            return MissilesToRender;
        }
    }
}
