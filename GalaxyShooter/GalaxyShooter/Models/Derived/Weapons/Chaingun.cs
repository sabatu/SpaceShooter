﻿using GalaxyShooter.Models.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Models.Derived.Weapons
{
    public class Chaingun : Weapon
    {
        //Call base constructor for initializing weapon to a parent ship.
        public Chaingun(int Damage,
                           GetShipXCoords GetXCoords,
                           GetShipYCoords GetYCoords,
                           string ProjectileColor,
                           float YVelocityOfProjectile) :base(Damage,
                           GetXCoords,
                           GetYCoords,
                           ProjectileColor,
                           YVelocityOfProjectile)
        {

        }

        //Call base constructor for initializing weapon to dead space.

        public Chaingun(float x,
                           float y,
                           float xVelocity,
                           float yVelocity,
                           string ProjectileColor,
                           int Damage,
                           float YVelocityOfProjectile) : base(x,
                           y,
                           xVelocity,
                           yVelocity,
                           ProjectileColor,
                           Damage,
                           YVelocityOfProjectile)
        {
            this.weaponSprite = Globals.Instance.GameContent.ChainGunDrop;
        }

        private List<Projectile> BulletsToRender = new List<Projectile>();

        public override void FireWeapon(Boolean withSound,
                                        int XDiff = 0)
        {
            Bullet FiredBullet = new Bullet(BaseDamage,
                                        getX() + XDiff,
                                        getY(),
                                         0,
                                         this.ProjectileYVel,
                                         this.ColorOfProjectile);

            if (withSound)
            {
                FiredBullet.MakeSound();
            }

            FiredBullet.Visible = true;

            BulletsToRender.Add(FiredBullet);
        }

        public override List<Projectile> ReturnProjectilesToRender()
        {
            return BulletsToRender;
        }


    }
}
