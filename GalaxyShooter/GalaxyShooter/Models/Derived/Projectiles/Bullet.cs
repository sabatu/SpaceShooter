﻿using GalaxyShooter.Interfaces;
using GalaxyShooter.Models.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Models
{
    public class Bullet : Projectile
    {
        public Bullet(int HPDamage,
             float X,
             float Y,
             float xVelocity,
             float yVelocity,
             string BulletColor)
        {
            this.x = X;
            this.y = Y;
            this.xVelocity = xVelocity;
            this.yVelocity = yVelocity;
            this.Visible = false;
            this.WaitingDraw = true;
            this.OffScreen = false;
            this.ProjectileHPDamage = HPDamage;
            this.StruckObject = false;

            //Set laser texture based on color passed in.
            switch (BulletColor)

            {
                case "Green":
                    {
                        ProjectileTexture = Globals.Instance.GameContent.BulletGreen;
                        Width = ProjectileTexture.Width;
                        Height = ProjectileTexture.Height;
                        break;
                    }
                case "Red":
                    {
                        ProjectileTexture = Globals.Instance.GameContent.BulletRed;
                        Width = ProjectileTexture.Width;
                        Height = ProjectileTexture.Height;
                        break;
                    }
                case "Yellow":
                    {
                        ProjectileTexture = Globals.Instance.GameContent.BulletYellow;
                        Width = ProjectileTexture.Width;
                        Height = ProjectileTexture.Height;
                        break;
                    }


                default:
                    throw new ApplicationException(string.Format("Cannot create bullet {0}", BulletColor));
            }

        }

        public override void MakeSound()
        {
            Globals.PlaySound(Globals.Instance.GameContent.GunFire);
        }

    }
}
