﻿using GalaxyShooter.Interfaces;
using GalaxyShooter.Models.Abstracts;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Models
{
    //What should launch event do?

    public class Laser : Projectile
    {  
        public Laser(int HPDamage,
                     float X,
                     float Y,
                     float xVelocity,
                     float yVelocity,
                     string LaserColor)
        {
            this.x = X;
            this.y = Y;
            this.xVelocity = xVelocity;
            this.yVelocity = yVelocity;   
            this.Visible = false;
            this.WaitingDraw = true;
            this.OffScreen = false;
            this.ProjectileHPDamage = HPDamage;
            this.StruckObject = false;

            //Set laser texture based on color passed in.
            switch (LaserColor)

            {
                case "Green":
                    {
                        ProjectileTexture = Globals.Instance.GameContent.GreenLaser;
                        Width = ProjectileTexture.Width;
                        Height = ProjectileTexture.Height;
                        break;
                    }
                case "Red":
                    {
                        ProjectileTexture = Globals.Instance.GameContent.RedLaser;
                        Width = ProjectileTexture.Width;
                        Height = ProjectileTexture.Height;
                        break;
                    }
                case "Blue":
                    {
                        ProjectileTexture = Globals.Instance.GameContent.BlueLaser;
                        Width = ProjectileTexture.Width;
                        Height = ProjectileTexture.Height;
                        break;
                    }
                case "BrightOrange":
                    {
                        ProjectileTexture = Globals.Instance.GameContent.BrightOrangeLaser;
                        Width = ProjectileTexture.Width;
                        Height = ProjectileTexture.Height;
                        break;
                    }
                case "DarkOrange":
                    {
                        ProjectileTexture = Globals.Instance.GameContent.DarkOrangeLaser;
                        Width = ProjectileTexture.Width;
                        Height = ProjectileTexture.Height;
                        break;
                    }
                case "Purple":
                    {
                        ProjectileTexture = Globals.Instance.GameContent.PurpleLaser;
                        Width = ProjectileTexture.Width;
                        Height = ProjectileTexture.Height;
                        break;
                    }
                case "Teal":
                    {
                        ProjectileTexture = Globals.Instance.GameContent.TealLaser;
                        Width = ProjectileTexture.Width;
                        Height = ProjectileTexture.Height;
                        break;
                    }
                case "Yellow":
                    {
                        ProjectileTexture = Globals.Instance.GameContent.YellowLaser;
                        Width = ProjectileTexture.Width;
                        Height = ProjectileTexture.Height;
                        break;
                    }

                default:
                    throw new ApplicationException(string.Format("Cannot create laser {0}", LaserColor));
            }

        }

        public override void MakeSound()
        {            
            Globals.PlaySound(Globals.Instance.GameContent.LaserFire);            
        }

    }
}
