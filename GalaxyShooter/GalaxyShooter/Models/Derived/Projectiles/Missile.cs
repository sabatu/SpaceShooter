﻿using GalaxyShooter.Interfaces;
using GalaxyShooter.Models.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Models
{
    class Missile : Projectile
    {
        public Missile(int HPDamage,
         float X,
         float Y,
         float xVelocity,
         float yVelocity,
         string MissileOrientation)
            {
                this.x = X;
                this.y = Y;
                this.xVelocity = xVelocity;
                this.yVelocity = yVelocity;
                this.Visible = false;
                this.WaitingDraw = true;
                this.OffScreen = false;
                this.ProjectileHPDamage = HPDamage;
                this.StruckObject = false;

                //Set laser texture based on color passed in.
                switch (MissileOrientation)

                {
                    case "Up":
                        {
                            ProjectileTexture = Globals.Instance.GameContent.MissileTowardsEnemy;
                            Width = ProjectileTexture.Width;
                            Height = ProjectileTexture.Height;
                            break;
                        }
                    case "Down":
                        {
                            ProjectileTexture = Globals.Instance.GameContent.MissileTowardsPlayer;
                            Width = ProjectileTexture.Width;
                            Height = ProjectileTexture.Height;
                            break;
                        }                    

                    default:
                        throw new ApplicationException(string.Format("Cannot create missile {0}", MissileOrientation));
                }

        }

        public override void MakeSound()
        {
            Globals.PlaySound(Globals.Instance.GameContent.RocketLaunch);
        }
    }
}
