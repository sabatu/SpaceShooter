﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaxyShooter.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GalaxyShooter.Observers;
using GalaxyShooter.Models.Abstracts;

namespace GalaxyShooter.Models
{
    public delegate float GetShipXCoords();
    public delegate float GetShipYCoords();

    public class Ship : PlayerScoreSubject, Interfaces.BaseShip
    {

        public float X { get; set; } //x position of ship on screen
        public float Y { get; set; } //y position of ship on screen
        public float XVelocity { get; set; }
        public float YVelocity { get; set; }
        public float Width { get; set; } //width of ship
        public float Height { get; set; } //height of ship
        public bool Visible { get; set; } //does ship still exist?
        public bool Dead { get; set; }        
        public bool WaitingDraw { get; set; }
        public List<Weapon> ShipWeapons { get; set; }
        protected Texture2D imgShip { get; set; }  //cached image of the ship
        public Boolean OffScreen = false;
        private List<Object> ObjectsToRender = new List<Object>();
        public int PlayerScoreBenchmark = 0;
        private int CurrentScore = 0;


        public int Score
        {
            get
            {
                return this.CurrentScore;

            }

            set
            {
                //Notifies observer (which notifies WeaponController method) when the user should be
                //rewarded with a weapon-power up. Basic logic is that user will get inital weapon powerup
                //at 1000 points, then another power-up everytime they double their score past 1000.
                //Score only gets incremented for player, and not enemy -- so enemies will not spawn their own powerups.

                this.CurrentScore = value;

                if (PlayerScoreBenchmark == 0 && this.CurrentScore >= 1000)
                {   
                    PlayerScoreBenchmark = this.CurrentScore;

                    //Notify observer that player has doubled their score (which will cause WeaponController to drop power-up.)
                     NotifyAllObservers();
                }
                else if (PlayerScoreBenchmark > 1000 && this.CurrentScore > (PlayerScoreBenchmark * 2))
                {
                    PlayerScoreBenchmark = this.CurrentScore;

                    //Notify observer that player has doubled their score (which will cause WeaponController to drop power-up.)
                    NotifyAllObservers();
                }
            }

        }


        public int ShipHP;
        public string ShipName;



        
        public Ship(float x,
                       float y,
                       float xVel,
                       float yVel,                       
                       Texture2D SpriteImage,
                       string LaserColor,
                       float YVelocityOfLaser,
                       int HP,
                       int DefaultWeaponDamage) : base() 

        {
                X = x;
                Y = y;
                XVelocity = xVel;
                YVelocity = yVel;                

                Dead = false;            
                WaitingDraw = true;            
                imgShip = SpriteImage;
                ShipName = SpriteImage.Name;

                Width = imgShip.Width;
                Height = imgShip.Height;
                Visible = true;

                this.ShipHP = HP;

            GetShipXCoords getXCoords = new GetShipXCoords(this.GetX);
            GetShipYCoords getYCoords = new GetShipYCoords(this.GetY);

   
             ShipWeapons = new List<Weapon> {new LaserWeapon(DefaultWeaponDamage,
                                           getXCoords,
                                           getYCoords,
                                           LaserColor,
                                           YVelocityOfLaser) };

            this.Score = 0;




        }

        public bool isShipDead()
        {
            return Dead;
        }


        public void Draw()
        {

            Globals.Instance.spriteBatch.Draw(imgShip,
                             new Vector2(X,
                                         Y),
                             null,
                             Color.White,
                             0,
                             new Vector2(0, 0),
                             1.0f,
                             SpriteEffects.None,
                             0);

            WaitingDraw = false;

        }

        public void Move()
        {
            X = (float)(((X + XVelocity)));
            Y = (float)(((Y + YVelocity)));

            if ((X < 1 || X > Globals.Instance.ScreenWidth) && WaitingDraw == false)
            {
                OffScreen = true;
            }
            if ((Y < 1 || Y > Globals.Instance.ScreenHeight) && WaitingDraw == false)
            {
                OffScreen = true;
            }
        }

        public void FireWeapon(Boolean withSound)
        {
            //Keeps projectiles from stacking on top of each other for multiple weapons.
            int xDifferential = 0;

            foreach(Weapon ShipWeapon in ShipWeapons)
            {
                ShipWeapon.FireWeapon(withSound, xDifferential);
                xDifferential += 15;
            }                
        } 

        public void AddWeapon(Weapon WeaponDrop)
        {
            //Affixes weapon to ship (passes delegates for getX and getY so weapon system understands where it's mounted now.)
            WeaponDrop.AttachSelfToShip(this);

            //Adds droppable weapon to list of ship weapons.
            ShipWeapons.Add(WeaponDrop);
        }

        public List<Object> ReturnObjectsToRender()
        {
            return ObjectsToRender;
        }

        public float GetX()
        {
            return this.X;
        }

        public float GetY()
        {
            return this.Y;
        }

    }
}
