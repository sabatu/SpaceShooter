﻿using GalaxyShooter.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Models.Abstracts
{
    public abstract class Weapon
    {
        public float X;
        public float Y;
        public float XVelocity { get; set; }
        public float YVelocity { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }
        public bool Visible { get; set; }  //is projectile visible on screen        
        public int BaseDamage { get; set; }
        public Texture2D weaponSprite { get; set; }
        public delegate float getParentX();
        public delegate float getParentY();
        public getParentX getX;
        public getParentY getY;
        public string ColorOfProjectile;
        public float ProjectileYVel;
        public bool OffScreen;
        public bool WaitingDraw { get; set; }


        //This constructor is for weapons which get initialized as part of a ship's construction.
        protected Weapon(int Damage,
                           GetShipXCoords GetXCoords,
                           GetShipYCoords GetYCoords,
                           string ProjectileColor,
                           float YVelocityOfProjectile)
        {
            XVelocity = 0;
            YVelocity = 0;
            Visible = false;      
            this.BaseDamage = Damage;

            getX = new getParentX(GetXCoords);
            getY = new getParentY(GetYCoords);

            this.ColorOfProjectile = ProjectileColor;
            this.ProjectileYVel = YVelocityOfProjectile;
        }

        //This constructor is for weapons that get initialized and dropped immediately into dead space, with the potential for getting picked up later.
        protected Weapon(float x,
                           float y,
                           float xVelocity,
                           float yVelocity,
                           string ProjectileColor,
                           int Damage,
                           float YVelocityOfProjectile)
        {
            this.X = x;
            this.Y = y;
            this.XVelocity = xVelocity;
            this.YVelocity = yVelocity;
            Visible = false;        
            this.BaseDamage = Damage;
            OffScreen = false;
            WaitingDraw = true;




            this.ColorOfProjectile = ProjectileColor;
            this.ProjectileYVel = YVelocityOfProjectile;
        }

        public void Draw()
        {
            Globals.Instance.spriteBatch.Draw(weaponSprite,
                             new Vector2(X, Y),
                             null,
                             Color.White,
                             0,
                             new Vector2(Width / 2, Height / 2),
                             1.0f,
                             SpriteEffects.None,
                             0);

            WaitingDraw = false;
        }

        public bool Move()
        {
            X = X + XVelocity;
            Y = Y + YVelocity;

            if ((X < 1 || X > Globals.Instance.ScreenWidth) && WaitingDraw == false)
            {
                OffScreen = true;
            }
            if ((Y < 1 || Y > Globals.Instance.ScreenHeight) && WaitingDraw == false)
            {
                OffScreen = true;
            }

            return true;
        }

        public void AttachSelfToShip(Ship PlayerShip)
        {
            getX = new getParentX(PlayerShip.GetX);
            getY = new getParentY(PlayerShip.GetY);
        }


        public abstract void FireWeapon(Boolean withSound,
                                        int XDiff = 0);

        public abstract List<Projectile> ReturnProjectilesToRender();

    }
}
