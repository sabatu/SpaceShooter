﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Models.Abstracts
{
    public abstract class Projectile
    {
        public bool OffScreen;
        public bool StruckObject;
        public float x;
        public float y;
        public float xVelocity;
        public float yVelocity;
        public int Width;
        public int Height;
        public bool WaitingDraw;
        public bool Visible { get; set; }
        public Texture2D ProjectileTexture { get; set; }
        public int ProjectileHPDamage;
 
        public bool Move()
        {
            x = x + xVelocity;
            y = y + yVelocity;

            if ((x < 1 || x > Globals.Instance.ScreenWidth) && WaitingDraw == false)
            {
                OffScreen = true;
            }
            if ((y < 1 || y > Globals.Instance.ScreenHeight) && WaitingDraw == false)
            {
                OffScreen = true;
            }

            return true;
        }
        public void Draw()
        {
            Globals.Instance.spriteBatch.Draw(ProjectileTexture,
                 new Vector2(this.x, this.y),
                 null,
                 Color.White,
                 0,
                 new Vector2(Width / 2, Height / 2),
                 1.0f,
                 SpriteEffects.None,
                 0);

            WaitingDraw = false;
        }

        public abstract void MakeSound();
    }
}
