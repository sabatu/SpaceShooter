﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace GalaxyShooter
{
    public class GameContent
    {
           
        public Texture2D RedLaser { get; set; }
        public Texture2D GreenLaser { get; set; }
        public Texture2D imgPixel { get; set; }
        public Texture2D PlayerShip { get; set; }
        public Texture2D LivesRemainingIcon { get; set; }
        public Texture2D EnemyShip1 { get; set; }
        public Texture2D EnemyShip2 { get; set; }
        public Texture2D EnemyShip3 { get; set; }
        public Texture2D EnemyShip4 { get; set; }
        public Texture2D SpaceBackground { get; set; }
        public Texture2D MidBoss { get; set; }
        public Texture2D FinalBoss { get; set; }

        public Texture2D BlueLaser { get; set; }
        public Texture2D BrightOrangeLaser { get; set; }
        public Texture2D DarkOrangeLaser { get; set; }
        public Texture2D PurpleLaser { get; set; }
        public Texture2D TealLaser { get; set; }
        public Texture2D YellowLaser { get; set; }

        public SoundEffect LaserFire { get; set; }
        public SoundEffect LongExplosion { get; set; }

        public SoundEffect EightBitJam { get; set; }
        public Texture2D BulletGreen { get; set; }
        public Texture2D BulletRed { get; set; }
        public Texture2D BulletYellow { get; set; }
        public SoundEffect EvilTemple { get; set; }
        public SoundEffect GunFire { get; set; }
        public Texture2D MissileTowardsEnemy { get; set; }
        public Texture2D MissileTowardsPlayer { get; set; }
        public SoundEffect RocketLaunch { get; set; }

        public Texture2D ChainGunDrop { get; set; }
        public Texture2D LaserDrop { get; set; }
        public Texture2D MissileDrop { get; set; }





        public SpriteFont labelFont { get; set; }

        public GameContent(ContentManager Content)
        {
            //load images
           
            imgPixel = Content.Load<Texture2D>("Pixel");


            MidBoss = Content.Load<Texture2D>("midboss");
            FinalBoss = Content.Load<Texture2D>("finalboss");

            PlayerShip = Content.Load<Texture2D>("PlayerShip");
            LivesRemainingIcon = Content.Load<Texture2D>("LivesRemainingIcon");
            EnemyShip1 = Content.Load<Texture2D>("EnemyShip1");
            EnemyShip2 = Content.Load<Texture2D>("EnemyShip2");
            EnemyShip3 = Content.Load<Texture2D>("EnemyShip3");
            EnemyShip4 = Content.Load<Texture2D>("EnemyShip4");
            RedLaser = Content.Load<Texture2D>("LaserRed");
            GreenLaser = Content.Load<Texture2D>("GreenLaser");
            BlueLaser = Content.Load<Texture2D>("BlueLaser");
            BrightOrangeLaser = Content.Load<Texture2D>("BrightOrange");
            DarkOrangeLaser = Content.Load<Texture2D>("DarkOrangeLaser");
            PurpleLaser = Content.Load<Texture2D>("PurpleLaser");
            TealLaser = Content.Load<Texture2D>("TealLaser");
            YellowLaser = Content.Load<Texture2D>("YellowLaser");

            EightBitJam = Content.Load<SoundEffect>("8BitJam");
            BulletGreen = Content.Load<Texture2D>("BulletGreen");
            BulletRed = Content.Load<Texture2D>("BulletRed");
            BulletYellow = Content.Load<Texture2D>("BulletYellow");
            EvilTemple = Content.Load<SoundEffect>("EvilTemple");
            GunFire = Content.Load<SoundEffect>("GunFire");
            MissileTowardsEnemy = Content.Load<Texture2D>("MissileTowardsEnemy");
            MissileTowardsPlayer = Content.Load<Texture2D>("MissileTowardsPlayer");
            RocketLaunch = Content.Load<SoundEffect>("rocket_launch");

            MissileDrop = Content.Load<Texture2D>("MissileDrop");
            ChainGunDrop = Content.Load<Texture2D>("ChainGunDrop");
            LaserDrop = Content.Load<Texture2D>("LaserDrop");


            SpaceBackground = Content.Load<Texture2D>("SpaceBackground");

            //load sounds

            LaserFire = Content.Load<SoundEffect>("LaserFire");
            LongExplosion = Content.Load<SoundEffect>("LongExplosion");

            //load fonts
            labelFont = Content.Load<SpriteFont>("Arial20");

        }
    }
}
