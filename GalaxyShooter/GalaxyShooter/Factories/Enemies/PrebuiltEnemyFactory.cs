﻿using GalaxyShooter.Interfaces;
using GalaxyShooter.JSONInterpreter;
using GalaxyShooter.Models;
using GalaxyShooter.Models.Abstracts;
using GalaxyShooter.Models.Derived.Weapons;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter
{
    public class PrebuiltEnemyFactory : NPCFactory, EnemyCreationStrategy
    {
        private Random EnemyShipY = new Random();
        private Random RandVelocity = new Random();

        public List<Ship> GetRegularEnemy(string type, int SizeOfGroup)
        {            
            switch (type)
            {
                case "ZigZagger":

                    List<Ship> ZigZagGroup = new List<Ship>();
                    for (int i = 0; i < SizeOfGroup; i++)
                    {
                        ZigZagGroup.Add(new Ship(50 * (i + 1),   //Controls pattern that group will appear in
                                                  EnemyShipY.Next(60),
                                                  RandVelocity.Next(-4, 5),
                                                  RandVelocity.Next(4, 5),
                                                  Globals.Instance.GameContent.EnemyShip1,
                                                  "Green",
                                                  1.2f,
                                                  20,
                                                  1));
                    }

                    return ZigZagGroup;

                case "Darter":

                    List<Ship> DarterGroup = new List<Ship>();
                    for (int i = 0; i < SizeOfGroup; i++)
                    {
                        DarterGroup.Add(new Ship(20 * (i + 1),   //Controls pattern that group will appear in
                                                  EnemyShipY.Next(30),
                                                  RandVelocity.Next(-3, 6),
                                                  RandVelocity.Next(2, 5),
                                                  Globals.Instance.GameContent.EnemyShip2,
                                                  "Blue",
                                                  3,
                                                  30,
                                                  1
                                                  ));
                    }

                    return DarterGroup;

                case "Tank":

                    List<Ship> TankGroup = new List<Ship>();
                    for (int i = 0; i < SizeOfGroup; i++)
                    {
                        TankGroup.Add(new Ship(80 * (i + 1),   //Controls pattern that group will appear in
                                                  EnemyShipY.Next(100),
                                                  2,
                                                  2,
                                                  Globals.Instance.GameContent.EnemyShip3,
                                                  "Purple",                                                  
                                                  4,
                                                  15,
                                                  1
                                                  ));
                    }
                    return TankGroup;

                case "Beeline":

                    List<Ship> BeeLineGroup = new List<Ship>();
                    for (int i = 0; i < SizeOfGroup; i++)
                    {
                        BeeLineGroup.Add(new Ship(40 * (i + 1),   //Controls pattern that group will appear in
                                                  EnemyShipY.Next(5),
                                                  0,
                                                  7,
                                                  Globals.Instance.GameContent.EnemyShip4,
                                                  "Teal",
                                                  2.2f,
                                                  5,
                                                  1));
                    }


                    return BeeLineGroup;

                default:
                    throw new ApplicationException(string.Format("Cannot create enemy {0}", type));
            }
        } 

        public Ship GetBoss(string type)
        {
            WeaponClient ExtraBossWeapons = new WeaponClient();

            switch (type)
            {
                case "midboss":

                      Ship midboss = new Ship(100,
                                       10,
                                       0,
                                       (float).5,
                                       Globals.Instance.GameContent.MidBoss,
                                       "Yellow",
                                       15,
                                       500,
                                       1);

                    //Assign midboss 3 extra random weapons 

     
                        midboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyPrebuiltWeapon());
                        midboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyPrebuiltWeapon());
                        midboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyPrebuiltWeapon());
                    

                    return midboss;


                case "finalboss":

                    Ship finalboss = new Ship(300,
                                       20,
                                       0,
                                       (float).25,
                                       Globals.Instance.GameContent.FinalBoss,
                                       "DarkOrange",
                                       5,
                                       1000,
                                       1);

                    //Assign final boss 6 extra random weapons              
                        finalboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyPrebuiltWeapon());
                        finalboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyPrebuiltWeapon());
                        finalboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyPrebuiltWeapon());
                        finalboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyPrebuiltWeapon());
                        finalboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyPrebuiltWeapon());
                        finalboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyPrebuiltWeapon());
                    

                    return finalboss;

                default:
                    throw new ApplicationException(string.Format("Cannot create enemy {0}", type));

            }
        }
    }
}
