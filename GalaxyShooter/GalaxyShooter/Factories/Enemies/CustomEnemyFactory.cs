﻿using GalaxyShooter.Interfaces;
using GalaxyShooter.JSONInterpreter;
using GalaxyShooter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.NPCs.Factories
{
    public class CustomEnemyFactory : NPCFactory , EnemyCreationStrategy
    {
        private Random EnemyShipY = new Random();
        private Random RandVelocity = new Random();
        
        List<CustomBehavior> BehaviorsToLoad = Globals.Instance.LoadedBehaviors;

        public List<Ship> GetRegularEnemy(string type, int SizeOfGroup)
        {
            List<CustomBehavior> BehaviorsToLoad = Globals.Instance.LoadedBehaviors;

            switch (type)
            {
                case "ZigZagger":

                    CustomBehavior CustomZigZagger = BehaviorsToLoad.Find(x => x.EnemyName == "ZigZagger");

                    List<Ship> ZigZagGroup = new List<Ship>();
                    for (int i = 0; i < SizeOfGroup; i++)
                    {
                        ZigZagGroup.Add(new Ship(CustomZigZagger.HorizontalSpacing * (i + 1),   //Controls pattern that group will appear in
                                                  EnemyShipY.Next(CustomZigZagger.VerticalSpacing),
                                                  RandVelocity.Next(CustomZigZagger.YVelocityMinInterval, CustomZigZagger.YVelocityMaxInterval),
                                                  RandVelocity.Next(CustomZigZagger.XVelocityMinInterval, CustomZigZagger.XVelocityMaxInterval),
                                                  Globals.Instance.GameContent.EnemyShip1,
                                                  CustomZigZagger.LaserColor,
                                                  CustomZigZagger.LaserVelocity,
                                                  CustomZigZagger.EnemyHP,
                                                  CustomZigZagger.DefaultWeaponDamage));
                    }

                    return ZigZagGroup;

                case "Darter":

                    CustomBehavior CustomDarter = BehaviorsToLoad.Find(x => x.EnemyName == "Darter");

                    List<Ship> DarterGroup = new List<Ship>();
                    for (int i = 0; i < SizeOfGroup; i++)
                    {
                        DarterGroup.Add(new Ship(CustomDarter.HorizontalSpacing * (i + 1),   //Controls pattern that group will appear in
                                                  EnemyShipY.Next(CustomDarter.VerticalSpacing),
                                                  RandVelocity.Next(CustomDarter.YVelocityMinInterval, CustomDarter.YVelocityMaxInterval),
                                                  RandVelocity.Next(CustomDarter.XVelocityMinInterval, CustomDarter.XVelocityMaxInterval),
                                                  Globals.Instance.GameContent.EnemyShip2,
                                                  CustomDarter.LaserColor,
                                                  CustomDarter.LaserVelocity,
                                                  CustomDarter.EnemyHP,
                                                  CustomDarter.DefaultWeaponDamage));
                    }

                    return DarterGroup;

                case "Tank":

                    CustomBehavior CustomTank = BehaviorsToLoad.Find(x => x.EnemyName == "Tank");

                    List<Ship> TankGroup = new List<Ship>();
                    for (int i = 0; i < SizeOfGroup; i++)
                    {
                        TankGroup.Add(new Ship(CustomTank.HorizontalSpacing * (i + 1),   //Controls pattern that group will appear in
                                                  EnemyShipY.Next(CustomTank.VerticalSpacing),
                                                  RandVelocity.Next(CustomTank.YVelocityMinInterval, CustomTank.YVelocityMaxInterval),
                                                  RandVelocity.Next(CustomTank.XVelocityMinInterval, CustomTank.XVelocityMaxInterval),
                                                  Globals.Instance.GameContent.EnemyShip3,
                                                  CustomTank.LaserColor,
                                                  CustomTank.LaserVelocity,
                                                  CustomTank.EnemyHP,
                                                  CustomTank.DefaultWeaponDamage));
                    }
                    return TankGroup;

                case "Beeline":

                    CustomBehavior CustomBeeLine = BehaviorsToLoad.Find(x => x.EnemyName == "Tank");

                    List<Ship> BeeLineGroup = new List<Ship>();
                    for (int i = 0; i < SizeOfGroup; i++)
                    {
                        BeeLineGroup.Add(new Ship(CustomBeeLine.HorizontalSpacing * (i + 1),   //Controls pattern that group will appear in
                                                  EnemyShipY.Next(CustomBeeLine.VerticalSpacing),
                                                  RandVelocity.Next(CustomBeeLine.YVelocityMinInterval, CustomBeeLine.YVelocityMaxInterval),
                                                  RandVelocity.Next(CustomBeeLine.XVelocityMinInterval, CustomBeeLine.XVelocityMaxInterval),
                                                  Globals.Instance.GameContent.EnemyShip4,
                                                  CustomBeeLine.LaserColor,
                                                  CustomBeeLine.LaserVelocity,
                                                  CustomBeeLine.EnemyHP,
                                                  CustomBeeLine.DefaultWeaponDamage));
                    }


                    return BeeLineGroup;

                default:
                    throw new ApplicationException(string.Format("Cannot create enemy {0}", type));
            }
        }

        public Ship GetBoss(string type)
        {
            List<CustomBehavior> BehaviorsToLoad = Globals.Instance.LoadedBehaviors;
            WeaponClient ExtraBossWeapons = new WeaponClient();

            switch (type)
            {
                

                case "midboss":

                    CustomBehavior CustomMidboss = BehaviorsToLoad.Find(x => x.EnemyName == "midboss");

                    Ship midboss = new Ship(CustomMidboss.HorizontalSpacing,   
                                                  CustomMidboss.VerticalSpacing,
                                                  RandVelocity.Next(CustomMidboss.YVelocityMinInterval, CustomMidboss.YVelocityMaxInterval),
                                                  RandVelocity.Next(CustomMidboss.XVelocityMinInterval, CustomMidboss.XVelocityMaxInterval),
                                                  Globals.Instance.GameContent.MidBoss,
                                                  CustomMidboss.LaserColor,
                                                  CustomMidboss.LaserVelocity,
                                                  CustomMidboss.EnemyHP,
                                                  CustomMidboss.DefaultWeaponDamage);

                    midboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyCustomWeapon());
                    midboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyCustomWeapon());
                    midboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyCustomWeapon());

                    return midboss;


                case "finalboss":

                    CustomBehavior CustomFinalBoss = BehaviorsToLoad.Find(x => x.EnemyName == "finalboss");

                    Ship finalboss =  new Ship(CustomFinalBoss.HorizontalSpacing,
                                                  CustomFinalBoss.VerticalSpacing,
                                                  RandVelocity.Next(CustomFinalBoss.YVelocityMinInterval, CustomFinalBoss.YVelocityMaxInterval),
                                                  RandVelocity.Next(CustomFinalBoss.XVelocityMinInterval, CustomFinalBoss.XVelocityMaxInterval),
                                                  Globals.Instance.GameContent.FinalBoss,
                                                  CustomFinalBoss.LaserColor,
                                                  CustomFinalBoss.LaserVelocity,
                                                  CustomFinalBoss.EnemyHP,
                                                  CustomFinalBoss.DefaultWeaponDamage);

                    finalboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyCustomWeapon());
                    finalboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyCustomWeapon());
                    finalboss.AddWeapon(ExtraBossWeapons.ReturnRandomEnemyCustomWeapon());

                    return finalboss;

                default:
                    throw new ApplicationException(string.Format("Cannot create boss {0}", type));

            }
        }
    }
}
