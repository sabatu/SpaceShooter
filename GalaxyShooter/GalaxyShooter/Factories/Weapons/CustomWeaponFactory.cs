﻿using GalaxyShooter.JSONInterpreter;
using GalaxyShooter.Models.Abstracts;
using GalaxyShooter.Models.Derived.Weapons;
using GalaxyShooter.Strategies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Factories
{
    class CustomWeaponFactory : WeaponCreationStrategy
    {
        public Weapon ReturnPlayerWeapon(string type)
        {
            List<CustomBehavior> BehaviorsToLoad = Globals.Instance.LoadedBehaviors;

            switch (type)
            {
                

                case "LaserWeapon":

                    CustomBehavior LaserWeapon = BehaviorsToLoad.Find(x => x.EnemyName == "LaserWeapon");

                    return new LaserWeapon(125,
                                           10,
                                           0,
                                           1,
                                           LaserWeapon.LaserColor,
                                           LaserWeapon.DefaultWeaponDamage,
                                           -20);


                case "Chaingun":

                    CustomBehavior Chaingun = BehaviorsToLoad.Find(x => x.EnemyName == "Chaingun");

                    return new Chaingun(125,
                                           10,
                                           0,
                                           1,
                                           Chaingun.LaserColor,
                                           Chaingun.DefaultWeaponDamage,
                                           -20);


                case "MissileLauncher":

                    CustomBehavior MissileLauncher = BehaviorsToLoad.Find(x => x.EnemyName == "MissileLauncher");

                    return new MissileLauncher(125,
                                           10,
                                           0,
                                           1,
                                           "Up",
                                           MissileLauncher.DefaultWeaponDamage,
                                           -20);



                default:
                    throw new ApplicationException(string.Format("Cannot create weapon {0}", type));


            }
        }

        public Weapon ReturnEnemyWeapon(string type)
        {
            List<CustomBehavior> BehaviorsToLoad = Globals.Instance.LoadedBehaviors;

            switch (type)
            {

                case "LaserWeapon":

                    CustomBehavior LaserWeapon = BehaviorsToLoad.Find(x => x.EnemyName == "LaserWeapon");

                    return new LaserWeapon(125,
                                           10,
                                           0,
                                           1,
                                           LaserWeapon.LaserColor,
                                           LaserWeapon.DefaultWeaponDamage,
                                           20);


                case "Chaingun":

                    CustomBehavior Chaingun = BehaviorsToLoad.Find(x => x.EnemyName == "Chaingun");

                    return new Chaingun(125,
                                           10,
                                           0,
                                           1,
                                           Chaingun.LaserColor,
                                           Chaingun.DefaultWeaponDamage,
                                           20);


                case "MissileLauncher":

                    CustomBehavior MissileLauncher = BehaviorsToLoad.Find(x => x.EnemyName == "MissileLauncher");

                    return new MissileLauncher(125,
                                           10,
                                           0,
                                           1,
                                           "Down",
                                           MissileLauncher.DefaultWeaponDamage,
                                           20);



                default:
                    throw new ApplicationException(string.Format("Cannot create weapon {0}", type));


            }
        }
    }
}
