﻿using GalaxyShooter.Models;
using GalaxyShooter.Models.Abstracts;
using GalaxyShooter.Models.Derived.Weapons;
using GalaxyShooter.Strategies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Factories
{
    public class PrebuiltWeaponFactory : WeaponCreationStrategy
    {
        public Weapon ReturnPlayerWeapon(string type)
        {
            switch(type)
            {

                case "LaserWeapon":

                    return new LaserWeapon(125,
                                           10,
                                           0,
                                           1,
                                           "Green",
                                           15,                                           
                                           -20);
                    

                case "Chaingun":

                    return new Chaingun(125,
                                           10,
                                           0,
                                           1,
                                           "Yellow",
                                           15,
                                           -20);
                    

                case "MissileLauncher":

                    return new MissileLauncher(125,
                                           10,
                                           0,
                                           1,
                                           "Up",
                                           15,
                                           -20);
                    


                default:
                    throw new ApplicationException(string.Format("Cannot create weapon {0}", type));


            }
        }

        public Weapon ReturnEnemyWeapon(string type)
        {
            switch (type)
            {

                case "LaserWeapon":

                    return new LaserWeapon(125,
                                           10,
                                           0,
                                           1,
                                           "Green",
                                           15,
                                           20);


                case "Chaingun":

                    return  new Chaingun(125,
                                           10,
                                           0,
                                           1,
                                           "Yellow",
                                           15,
                                           20);


                case "MissileLauncher":

                    return new MissileLauncher(125,
                                           10,
                                           0,
                                           1,
                                           "Down",
                                           15,
                                           20);



                default:
                    throw new ApplicationException(string.Format("Cannot create weapon {0}", type));


            }
        }
    }
}
