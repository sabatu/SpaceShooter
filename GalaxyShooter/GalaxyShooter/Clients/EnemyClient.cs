﻿using GalaxyShooter.Context;
using GalaxyShooter.Models;
using GalaxyShooter.NPCs.Factories;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter
{
    class EnemyClient
    {
        EnemyCreationContext PrebuiltEnemyContext = new EnemyCreationContext(new PrebuiltEnemyFactory());
        EnemyCreationContext CustomEnemyContext = new EnemyCreationContext(new CustomEnemyFactory());
       
        List<String> LowLevelEnemyTypes = new List<String>() { "ZigZagger",
                                                               "Darter",
                                                               "Tank",
                                                               "Beeline" };
        Random RandomSelection = new Random();
        int index = 0;

        //TO-DO: Implement boss type

        public EnemyClient()
        {   

        }

        public List<Ship> ReturnCustomRandomEnemyWave(int SizeOfGroup)
        {

            //Randomly select which type of enemy will be returned.
            index = RandomSelection.Next(LowLevelEnemyTypes.Count);           

            return CustomEnemyContext.executeEnemyCreationStrategy(LowLevelEnemyTypes[index],
                                                      SizeOfGroup);

            
        }

        public List<Ship> ReturnCustomExplicitEnemyWave(int SizeOfGroup,
                                 String enemyType)
        {
            return PrebuiltEnemyContext.executeEnemyCreationStrategy(enemyType,
                                          SizeOfGroup);
          

        }


        public List<Ship> ReturnRandomEnemyWave(int SizeOfGroup)
        {

            //Randomly select which type of enemy will be returned.
            index = RandomSelection.Next(LowLevelEnemyTypes.Count);

            return PrebuiltEnemyContext.executeEnemyCreationStrategy(LowLevelEnemyTypes[index],
                                                      SizeOfGroup);
            
        }

        public List<Ship> ReturnExplicitEnemyWave(int SizeOfGroup,
                                 String enemyType)
        {
            return PrebuiltEnemyContext.executeEnemyCreationStrategy(enemyType,
                                          SizeOfGroup);
            

        }

        public Ship ReturnPrebuiltBoss(string type)
        {
            return PrebuiltEnemyContext.executeBossCreationStrategy(type);        
        }

        public Ship ReturnCustomBoss(string type)
        {
            return CustomEnemyContext.executeBossCreationStrategy(type);
        }

        public List<String> ReturnLowLevelEnemyTypes()
        {
            return LowLevelEnemyTypes;
        }

    }
}
