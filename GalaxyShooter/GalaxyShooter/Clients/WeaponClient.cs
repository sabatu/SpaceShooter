﻿using GalaxyShooter.Context;
using GalaxyShooter.Factories;
using GalaxyShooter.Models;
using GalaxyShooter.Models.Abstracts;
using GalaxyShooter.NPCs.Factories;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter
{
    class WeaponClient
    {
        
        WeaponCreationContext PrebuiltWeaponContext = new WeaponCreationContext(new PrebuiltWeaponFactory());
        WeaponCreationContext CustomWeaponContext = new WeaponCreationContext(new CustomWeaponFactory());

        List<String> WeaponTypes = new List<String>() { "LaserWeapon",
                                                        "Chaingun",
                                                        "MissileLauncher" };
        Random RandomSelection = new Random();
        int index = 0;

        //TO-DO: Implement boss type

        public WeaponClient()
        {

        }

        //Prebuilt methods
        public Weapon ReturnRandomPlayerPrebuiltWeapon()
        {
            //Randomly select which type of weapon will be returned.
            index = RandomSelection.Next(WeaponTypes.Count);

            return PrebuiltWeaponContext.executePlayerWeaponCreationStrategy(WeaponTypes[index]);
        }

        public Weapon ReturnExplicitPlayerPrebuiltWeapon(string type)
        {
            return PrebuiltWeaponContext.executePlayerWeaponCreationStrategy(type);
        }

        public Weapon ReturnRandomEnemyPrebuiltWeapon()
        {
            //Randomly select which type of weapon will be returned.
            index = RandomSelection.Next(WeaponTypes.Count);

            return PrebuiltWeaponContext.executeEnemyWeaponCreationStrategy(WeaponTypes[index]);
        }

        public Weapon ReturnExplicitEnemyPrebuiltWeapon(string type)
        {
            return PrebuiltWeaponContext.executeEnemyWeaponCreationStrategy(type);
        }


        //Custom methods
        public Weapon ReturnRandomPlayerCustomWeapon()
        {
            //Randomly select which type of weapon will be returned.
            index = RandomSelection.Next(WeaponTypes.Count);

            return CustomWeaponContext.executePlayerWeaponCreationStrategy(WeaponTypes[index]);
        }

        public Weapon ReturnExplicitPlayerCustomWeapon(string type)
        {
            return CustomWeaponContext.executePlayerWeaponCreationStrategy(type);
        }

        public Weapon ReturnRandomEnemyCustomWeapon()
        {
            //Randomly select which type of weapon will be returned.
            index = RandomSelection.Next(WeaponTypes.Count);

            return CustomWeaponContext.executeEnemyWeaponCreationStrategy(WeaponTypes[index]);
        }

        public Weapon ReturnRandomEnemyCustomWeapon(string type)
        {
            return CustomWeaponContext.executeEnemyWeaponCreationStrategy(type);
        }






        public List<String> ReturnLowLevelEnemyTypes()
        {
            return WeaponTypes;
        }

    }
}
