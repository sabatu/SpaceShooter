﻿using GalaxyShooter.Controllers;
using GalaxyShooter.JSONInterpreter;
using GalaxyShooter.Models;
using GalaxyShooter.Models.Abstracts;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GalaxyShooter
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    /// public class titleScreen : Game
    /// 
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;

        private Ship LivesRemainingIcon;
        NPCFactory NPCFactory = new PrebuiltEnemyFactory();   

        private int timecheck = 0;
        private bool HasMidBossDisplayed = false;
        private bool HasFinalBossDisplayed = false;
        private bool MidBossKilled = false;
        private bool FinalBossKilled = false;   
        private bool PlayerDied = false;
        private int PlayerDiedAt = 0;
        private GameBackground spaceBackground;

        //Custom timing values.
        private int GameTimerSeconds = 0;
        private int GameTimerMilliSeconds = 0;

        private int InstanceResetSeconds = 0;
        private int InstanceResetMilliseconds = 0;
       

        private MouseState oldMouseState;
        private KeyboardState oldKeyboardState;
    
        private int livesRemaining;

        private EnemyController EnemyController;      
        private PlayerController PlayerController;
        private WeaponController WeaponController;
		
		// second player items
		private PlayerController Player2Controller;
		private bool Player2Died = true;
        private int Player2DiedAt = 0;
		private bool Player2Made = false;
		private int lives2Remaining = 0;
		
		// difficulty levels
		private bool level1 = true;
		private bool level2 = false, level3 = false;
		
        private HashSet<Ship> ActivePlayerOnScreen = new HashSet<Ship>();
        private HashSet<Ship> ActiveEnemiesOnScreen = new HashSet<Ship>();
        private HashSet<Projectile> ActivePlayerProjectilesOnScreen = new HashSet<Projectile>();
        private HashSet<Projectile> ActiveEnemyProjectilesOnScreen = new HashSet<Projectile>();

        public enum Screens
        {
            StartMenu,
            MainGame,
            EndGame,
            LoadJSONMenu
        }

        private Screens CurrentScreen;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";  
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            this.EnemyController = new EnemyController();
            this.PlayerController = new PlayerController();
            this.WeaponController = new WeaponController();

			// player 2
			this.Player2Controller = new PlayerController();
			
            GameTimerSeconds = 0;
            GameTimerMilliSeconds = 0;

            // TODO: Add your initialization logic here
            base.Initialize();

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            Globals.Instance.spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            Globals.Instance.GameContent = new GameContent(Content);
            
            Globals.Instance.ScreenWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            Globals.Instance.ScreenHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            //set game to 502x700 or screen max if smaller
            if (Globals.Instance.ScreenWidth >= 502)
            {
                Globals.Instance.ScreenWidth = 502;
            }
            if (Globals.Instance.ScreenHeight >= 700)
            {
                Globals.Instance.ScreenHeight = 700;
            }
            graphics.PreferredBackBufferWidth = Globals.Instance.ScreenWidth;
            graphics.PreferredBackBufferHeight = Globals.Instance.ScreenHeight;
            graphics.ApplyChanges();

            // Was thinking of putting the menu option in this location

            CurrentScreen = Screens.StartMenu;
            PlayerController.CreatePlayer(WeaponController);
            Player2Controller.CreatePlayer(WeaponController);

            spaceBackground = new GameBackground();         

        }

    

    /// <summary>
    /// UnloadContent will be called once per game and is the place to unload
    /// game-specific content.
    /// </summary>
    protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            if (IsActive == false)
            {
                return;  //our window is not active don't update
            }


            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            //process left-click


            KeyboardState newKeyboardState = Keyboard.GetState();
            MouseState newMouseState = Mouse.GetState();

            if (newMouseState.LeftButton == ButtonState.Released && oldMouseState.LeftButton == ButtonState.Pressed && oldMouseState.X == newMouseState.X && oldMouseState.Y == newMouseState.Y)
            {
                PlayerController.FireWeapons();  
            }
            
            if (newKeyboardState.IsKeyDown(Keys.Space))
            {
                PlayerController.FireWeapons();
            }
            //process mouse move                                
            if (oldMouseState.X != newMouseState.X)
            {
                if (newMouseState.X >= 0 || newMouseState.X < Globals.Instance.ScreenWidth)
                {
                    PlayerController.MoveTo(newMouseState.X,newMouseState.Y);
                }
            }

            if ((CurrentScreen == Screens.StartMenu) && (newKeyboardState.IsKeyDown(Keys.Space)))
            {
                //Allot 3 lives for initial play-through.
                livesRemaining = 3;
				
                CurrentScreen = Screens.MainGame;
            }
			// changing the initial level difficulty
			if ((CurrentScreen == Screens.StartMenu) && (newKeyboardState.IsKeyDown(Keys.D1)))
            {
				level1 = true;
				level2 = false;
				level3 = false;
            }
			if ((CurrentScreen == Screens.StartMenu) && (newKeyboardState.IsKeyDown(Keys.D2)))
            {
				level1 = false;
				level2 = true;
				level3 = false;
            }
			if ((CurrentScreen == Screens.StartMenu) && (newKeyboardState.IsKeyDown(Keys.D3)))
            {
				level1 = false;
				level2 = true;
				level3 = false;
            }

            if((newKeyboardState.IsKeyDown(Keys.D7)))
            {
                Globals.PlaySound(Globals.Instance.GameContent.EightBitJam);
            }

            if ((newKeyboardState.IsKeyDown(Keys.D8)))
            {
                Globals.PlaySound(Globals.Instance.GameContent.EvilTemple);
            }



            //Easily-activated cheat mode.
            if (newKeyboardState.IsKeyDown(Keys.C))
            {
                PlayerController.TogglePlayerInvincibility();
                ActivePlayerOnScreen.Clear();
                ActivePlayerOnScreen.Add(PlayerController.ReturnModelInstances().ElementAt(0));
            }

			// if this is another cheat mode maybe move a key,
			// have player 2 as IJKL for movements
            //Easily-activated cheat mode.
            if ((CurrentScreen == Screens.StartMenu) && newKeyboardState.IsKeyDown(Keys.J))
            {
                EnemyController.LoadCustomEnemies();

                //Allot 3 lives for initial play-through.
                livesRemaining = 3;

                CurrentScreen = Screens.MainGame;
            }



            if ((CurrentScreen == Screens.EndGame) && (newKeyboardState.IsKeyDown(Keys.R)))
            {
                //Clear and reset game state for next play-through.
                this.EnemyController = new EnemyController();
                this.PlayerController = new PlayerController();
                this.WeaponController = new WeaponController();

                // player 2 resets
                lives2Remaining = 0;
                Player2Died = true;
				Player2Made = false;

                //Reset this option to false.
                Globals.Instance.LoadCustomBehaviors = false;

                //Recreate player
                PlayerController.CreatePlayer(WeaponController);
                livesRemaining = 3;
                PlayerDied = false;
                ActivePlayerOnScreen.Clear();                
                ActivePlayerOnScreen.Add(PlayerController.ReturnModelInstances().First());

                //Reset boss conditionals
                MidBossKilled = false;
                FinalBossKilled = false;
                HasMidBossDisplayed = false;
                HasFinalBossDisplayed = false;

                InstanceResetSeconds = ((gameTime.TotalGameTime.Minutes * 60) + gameTime.TotalGameTime.Seconds);
                InstanceResetMilliseconds = gameTime.TotalGameTime.Milliseconds;

                //Set screen to main game.
                CurrentScreen = Screens.MainGame;
            }
            //process keyboard events                           
            if (newKeyboardState.IsKeyDown(Keys.Left))
            {
                PlayerController.MoveLeft();
            }
            if (newKeyboardState.IsKeyDown(Keys.Right))
            {
                PlayerController.MoveRight();
            }

            if (newKeyboardState.IsKeyDown(Keys.Up))
            {
                PlayerController.MoveUp();
            }

            if (newKeyboardState.IsKeyDown(Keys.Down))
            {
                PlayerController.MoveDown();
            }
			if (newKeyboardState.IsKeyDown(Keys.Z))
			{
				// cheat mode key press
				// this will add lives to the player,
				// used to be a cheat method in old arcades
				livesRemaining += 1;
			}

            if (newKeyboardState.IsKeyDown(Keys.E))
            {
                PlayerController.playerSpeedChangeUp();
                //TargetElapsedTime = TimeSpan.FromTicks(444444);
                // have the public classes in the player class to decrease the speed
                // but want to discuss the idea of multipliers
            }	
            //This will return the game to it's normal draw rate.
            if (newKeyboardState.IsKeyDown(Keys.Q))
            {
                Player2Controller.playerSpeedChangeDown();
                // same as previous
                //TargetElapsedTime = TimeSpan.FromTicks(111111);
            }
			
			//----------------------------------------------------------------
			// this section will be for the player2 creation
			if (newKeyboardState.IsKeyDown(Keys.N) && Player2Made == false)
            {
                Player2Controller.CreatePlayer(WeaponController);
                lives2Remaining = 3;
                Player2Died = false;
                Player2Made = true;
                ActivePlayerOnScreen.Add(Player2Controller.ReturnModelInstances().First());
            }
            // player2 keyboard movements
            if (newKeyboardState.IsKeyDown(Keys.U))
            {
                Player2Controller.FireWeapons();
            }
            if (newKeyboardState.IsKeyDown(Keys.J))
            {
                Player2Controller.MoveLeft();
            }
            if (newKeyboardState.IsKeyDown(Keys.L))
            {
                Player2Controller.MoveRight();
            }

            if (newKeyboardState.IsKeyDown(Keys.I))
            {
                Player2Controller.MoveUp();
            }

            if (newKeyboardState.IsKeyDown(Keys.K))
            {
                Player2Controller.MoveDown();
            }
            if (newKeyboardState.IsKeyDown(Keys.Y))
			{
				lives2Remaining += 1;
			}
            if (newKeyboardState.IsKeyDown(Keys.OemMinus))
            {
                Player2Controller.playerSpeedChangeUp();
            }
			if (newKeyboardState.IsKeyDown(Keys.OemPlus))
            {
                Player2Controller.playerSpeedChangeDown();
            }
			//------------------------------------------------------------
            oldMouseState = newMouseState; // this saves the old state      
            oldKeyboardState = newKeyboardState;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            Globals.Instance.spriteBatch.Begin();


            if (CurrentScreen == Screens.StartMenu)
            {
                GraphicsDevice.Clear(Color.Black);  //Change the GraphicsDevice.Clear method to use Color.Black as shown here

                GameTimerSeconds = 0;
                GameTimerMilliSeconds = 0;                

                string startMsg = "Press spacebar to Start, or press J to load custom level.";
                Vector2 startSpace = Globals.Instance.GameContent.labelFont.MeasureString(startMsg);
                Globals.Instance.spriteBatch.DrawString(Globals.Instance.GameContent.labelFont,
                                                        startMsg, new Vector2((Globals.Instance.ScreenWidth - startSpace.X) / 2,
                                                                               Globals.Instance.ScreenHeight / 2),
                                                                               Color.White);
                
            }

            if(CurrentScreen == Screens.LoadJSONMenu)
            {
                string startMsg = "Please select level to load from below.";
                Vector2 startSpace = Globals.Instance.GameContent.labelFont.MeasureString(startMsg);
                Globals.Instance.spriteBatch.DrawString(Globals.Instance.GameContent.labelFont,
                                                        startMsg, new Vector2((Globals.Instance.ScreenWidth - startSpace.X) / 2,
                                                                               Globals.Instance.ScreenHeight / 2),
                                                                               Color.White);

            }

            if (CurrentScreen == Screens.EndGame)
            {
                GraphicsDevice.Clear(Color.Black);  //Change the GraphicsDevice.Clear method to use Color.Black as shown here

                string endMsg = String.Empty;

                GameTimerSeconds = 0;
                GameTimerMilliSeconds = 0;

                if (FinalBossKilled)
                {
                    endMsg = "Congratulations -- you killed the final boss!!" + Environment.NewLine + " Press the R key to restart game, or press Esc to exit.";
                }
                else
                {
                    endMsg  = "Game Over. Press the R key to restart game, or press Esc to exit.";
                }


                Vector2 endSpace = Globals.Instance.GameContent.labelFont.MeasureString(endMsg);
                Globals.Instance.spriteBatch.DrawString(Globals.Instance.GameContent.labelFont,
                                                        endMsg, new Vector2((Globals.Instance.ScreenWidth - endSpace.X) / 2,
                                                                             Globals.Instance.ScreenHeight / 2),
                                                                             Color.White);
            }

            if (CurrentScreen == Screens.MainGame)
            {
                GraphicsDevice.Clear(Color.Black);  //Change the GraphicsDevice.Clear method to use Color.Black as shown here          

                //Keep instance timers set to MonoGame timer unless conditional logic changes.
                GameTimerSeconds = (((gameTime.TotalGameTime.Minutes * 60) + gameTime.TotalGameTime.Seconds) - InstanceResetSeconds);
                GameTimerMilliSeconds = (gameTime.TotalGameTime.Milliseconds - InstanceResetMilliseconds);

                spaceBackground.Draw();

                //Add player instance to ships on screen.   
                bool test = ActivePlayerOnScreen.Add(PlayerController.ReturnModelInstances().ElementAt(0));


                //Roughly every 600 milliseconds, add more enemies to the map.
				// level 1
                if (((HasMidBossDisplayed && MidBossKilled) || (!HasMidBossDisplayed)) &&
                    !HasFinalBossDisplayed && GameTimerSeconds != timecheck &&
                    GameTimerMilliSeconds % 600 == 0 && level1 == true && level2 == false && level3 == false)
                {
                    timecheck = gameTime.TotalGameTime.Seconds;
                    EnemyController.CreateRandomEnemyWave(NPCFactory, 5);
                }
				// level 2 
				if (((HasMidBossDisplayed && MidBossKilled) || (!HasMidBossDisplayed)) &&
                    !HasFinalBossDisplayed && GameTimerSeconds != timecheck &&
                    GameTimerMilliSeconds % 800 == 0 && level1 == false && level2 == true && level3 == false)
                {
                    timecheck = gameTime.TotalGameTime.Seconds;
                    EnemyController.CreateRandomEnemyWave(NPCFactory, 5);
                }
				// level 3
				if (((HasMidBossDisplayed && MidBossKilled) || (!HasMidBossDisplayed)) &&
                    !HasFinalBossDisplayed && GameTimerSeconds != timecheck &&
                    GameTimerMilliSeconds % 1000 == 0 && level1 == false && level2 == false && level3 == true)
                {
                    timecheck = gameTime.TotalGameTime.Seconds;
                    EnemyController.CreateRandomEnemyWave(NPCFactory, 5);
                }

                //Add the created enemies to the view's hashset (duplicate objects being added get rejected by the HashSet object.)
                foreach (Ship ActiveEnemyShip in EnemyController.ReturnModelInstances())
                {
                    ActiveEnemiesOnScreen.Add(ActiveEnemyShip);
                }

                //Display midboss at 48 seconds
                if (!HasMidBossDisplayed && GameTimerSeconds % 48 == 0 && GameTimerSeconds != 0)
                {
                    HasMidBossDisplayed = true;
                    EnemyController.CreateBoss(NPCFactory, "midboss");
                }

                //Final boss displays at 90 seconds.
                if (!HasFinalBossDisplayed && GameTimerSeconds % 90 == 0 && GameTimerSeconds != 0)
                {
                    HasFinalBossDisplayed = true;
                    EnemyController.CreateBoss(NPCFactory, "finalboss");
                }

                //Check to determine if player has successfully killed the midboss.
                if (HasMidBossDisplayed && !MidBossKilled &&
                    EnemyController.ReturnModelInstances().Exists(x => x.ShipName == "midboss") &&
                    EnemyController.ReturnModelInstances().Find(x => x.ShipName == "midboss").Dead == true)
                {
                    MidBossKilled = true;                    
                }

                //Check to determine if player has successfully beaten the game.
                if (HasFinalBossDisplayed && EnemyController.ReturnModelInstances().Find(x => x.ShipName =="finalboss").Dead == true)
                {
                    FinalBossKilled = true;
                    CurrentScreen = Screens.EndGame;
                }



                //Check to determine if player's ship is dead.
                if (PlayerController.ReturnModelInstances().First().isShipDead())
                {
                    livesRemaining -= 1;

                    //Reset player.
                    PlayerController.CreatePlayer(WeaponController);


                    PlayerDied = true;

                    HasMidBossDisplayed = false;
                    HasFinalBossDisplayed = false;

                    PlayerDiedAt = GameTimerSeconds;

                    //Reset enemies and lasers.
                    EnemyController.ReturnModelInstances().Clear();
                    EnemyController.ReturnProjectilesToRender().Clear();
                    ActiveEnemiesOnScreen.Clear();
                    ActiveEnemyProjectilesOnScreen.Clear();

                    ActivePlayerOnScreen.Clear();
                    PlayerController.CreatePlayer(WeaponController);
                    Player2Controller.CreatePlayer(WeaponController);
                    ActivePlayerOnScreen.Add(PlayerController.ReturnModelInstances().First());

                    InstanceResetSeconds = ((gameTime.TotalGameTime.Minutes * 60) + gameTime.TotalGameTime.Seconds);
                    InstanceResetMilliseconds = gameTime.TotalGameTime.Milliseconds;

                }
                if (Player2Controller.ReturnModelInstances().First().isShipDead())
                {
                    lives2Remaining -= 1;

                    //Reset player.
                    Player2Controller.CreatePlayer(WeaponController);


                    Player2Died = true;

                    HasMidBossDisplayed = false;
                    HasFinalBossDisplayed = false;

                    Player2DiedAt = GameTimerSeconds;

                    /*
                    Reset enemies and lasers.
                    EnemyController.ReturnModelInstances().Clear();
                    EnemyController.ReturnProjectilesToRender().Clear();
                    ActiveEnemiesOnScreen.Clear();
                    ActiveEnemyLasersOnScreen.Clear();

                    ActivePlayerOnScreen.Clear();
                    PlayerController.CreatePlayer(WeaponController);
                    ActivePlayerOnScreen.Add(PlayerController.ReturnModelInstances().First());

                    InstanceResetSeconds = ((gameTime.TotalGameTime.Minutes * 60) + gameTime.TotalGameTime.Seconds);
                    InstanceResetMilliseconds = gameTime.TotalGameTime.Milliseconds;*/

                }



                //Check to determine if player "invincibility status" can be lifted.
                if (PlayerDied && ((GameTimerSeconds - PlayerDiedAt) > 2))
                {
                    PlayerDied = false;
                    PlayerDiedAt = 0;
                }

                //Roughly every 1600 milliseconds, enemies fire weapons.
                if (!PlayerDied &&  GameTimerMilliSeconds % 1600 == 0)
                {
                    EnemyController.FireWeapons();
                }



                foreach (Ship ActiveEnemyShip in EnemyController.ReturnModelInstances())
                {
                    ActiveEnemiesOnScreen.Add(ActiveEnemyShip);
                }

                foreach (Projectile ActiveProjectile in PlayerController.ReturnProjectilesToRender())
                {
                    ActivePlayerProjectilesOnScreen.Add(ActiveProjectile);
                }

                foreach (Projectile ActiveProjectile in Player2Controller.ReturnProjectilesToRender())
                {
                    ActivePlayerProjectilesOnScreen.Add(ActiveProjectile);
                }


                //Only draw enemy lasers if player hasn't recently died, and midboss or final boss aren't currently on the screen.
                //if (!PlayerDied && !HasFinalBossDisplayed && !(HasMidBossDisplayed && !MidBossKilled))
                //{                    

                //    foreach (Laser ActiveLaser in EnemyController.ReturnProjectilesToRender())
                //    {
                //        ActiveEnemyProjectilesOnScreen.Add(ActiveLaser);
                //    }
                //}

                foreach (Projectile ActiveProjectile in EnemyController.ReturnProjectilesToRender())
                {
                    ActiveEnemyProjectilesOnScreen.Add(ActiveProjectile);
                }


                //Screen elements get rendered here.
                RenderShips(ActivePlayerOnScreen.ToList());
                RenderShips(ActiveEnemiesOnScreen.ToList());
                RenderProjectiles(ActivePlayerProjectilesOnScreen.ToList());
                RenderProjectiles(ActiveEnemyProjectilesOnScreen.ToList());

                RenderWeapons(WeaponController.ReturnWeaponsToRender());

                //Count ships that have 'died' and modify player score by a factor of 100 for each enemy killed.
                PlayerController.ModifyPlayerScore(100 * EnemyController.ReturnModelInstances().Count(X => X.Dead == true));

                //De-reference objects that are no longer visible on screen
                //(MonoGame will make sure they are internally removed from memory after de-referencing.)
                EnemyController.ReturnModelInstances().RemoveAll(X => (X.Dead || X.OffScreen) == true);
                EnemyController.ReturnProjectilesToRender().RemoveAll(X => (X.OffScreen || X.StruckObject) == true);
                PlayerController.ReturnProjectilesToRender().RemoveAll(X => (X.OffScreen || X.StruckObject) == true);
                Player2Controller.ReturnProjectilesToRender().RemoveAll(X => (X.OffScreen || X.StruckObject) == true);
                WeaponController.ReturnWeaponsToRender().RemoveAll(X => X.OffScreen == true);


                //Also remove end-of-lifecycle objects from hashsets used in collision detection.
                //ActivePlayerOnScreen.RemoveWhere(X => X.Dead == true);
                ActiveEnemiesOnScreen.RemoveWhere(X => (X.Dead || X.OffScreen) == true);
                ActivePlayerProjectilesOnScreen.RemoveWhere(X => (X.OffScreen || X.StruckObject) == true);
                ActiveEnemyProjectilesOnScreen.RemoveWhere(X => (X.OffScreen || X.StruckObject) == true);       

                //Creates hitboxes of all ships on screen, performs a hit test, and decrements ship HP
                //by base HP damage of laser.
                WeaponController.DetectCollisions(ActivePlayerOnScreen.ToList(),ActiveEnemyProjectilesOnScreen.ToList());
                WeaponController.DetectCollisions(ActiveEnemiesOnScreen.ToList(), ActivePlayerProjectilesOnScreen.ToList());

                string scoreMsg = "Score: " + PlayerController.ReturnPlayerScore().ToString();
                Vector2 space = Globals.Instance.GameContent.labelFont.MeasureString(scoreMsg);
                Globals.Instance.spriteBatch.DrawString(Globals.Instance.GameContent.labelFont,
                                                        scoreMsg,
                                                        new Vector2((Globals.Instance.ScreenWidth - space.X) / 2,
                                                        Globals.Instance.ScreenHeight - 40),
                                                        Color.White);


				// adding in live2remaining as a condition?
                if (livesRemaining > 0)
                {

                    Globals.Instance.spriteBatch.DrawString(Globals.Instance.GameContent.labelFont,
                                        livesRemaining.ToString(),
                                        new Vector2(40, 10),
                                        Color.White);
                }
                else
                {
                    CurrentScreen = Screens.EndGame;
                }                
            }

            Globals.Instance.spriteBatch.End();
            base.Draw(gameTime);

        }





        private void RenderWeapons(List<Weapon> WeaponsToRender)
        {
            foreach(Weapon weapon in WeaponsToRender)
            {
                weapon.Draw();
                weapon.Move();

                WeaponController.DetectWeaponCollisions(PlayerController.ReturnModelInstances().First(), weapon);
            }
        }

        private void RenderShips(List<Ship> ShipsToRender)
        {
            foreach(Ship Sprite in ShipsToRender)
            {
                Sprite.Draw();
                Sprite.Move();
            }
        }

        private void RenderProjectiles(List<Projectile> ProjectilesToRender)
        {
            foreach (Projectile Sprite in ProjectilesToRender)
            {
                Sprite.Draw();
                Sprite.Move();                
            }
        }
    }
}
