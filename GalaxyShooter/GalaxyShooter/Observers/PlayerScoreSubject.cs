﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Observers
{
    public abstract class PlayerScoreSubject
    {
        //subscribers
        private List<PlayerScoreObserver> observers = new List<PlayerScoreObserver>();

        public void attach(PlayerScoreObserver observer)
        {
            observers.Add(observer);
        }

        public void detach(PlayerScoreObserver observer)
        {
            observers.Remove(observer);
        }

        public void NotifyAllObservers()
        {
            foreach (PlayerScoreObserver observer in observers)
            {
                observer.DropWeaponPowerup();
            }
        }
    }
}
