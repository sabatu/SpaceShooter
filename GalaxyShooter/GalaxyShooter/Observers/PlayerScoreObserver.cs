﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Observers
{
    public abstract class PlayerScoreObserver
    {
        public PlayerScoreSubject subject;
        public abstract void DropWeaponPowerup();

    }
}
