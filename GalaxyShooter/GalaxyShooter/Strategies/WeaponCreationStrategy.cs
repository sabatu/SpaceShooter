﻿using GalaxyShooter.Models.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Strategies
{
    public interface WeaponCreationStrategy
    {
        Weapon ReturnPlayerWeapon(string type);
        Weapon ReturnEnemyWeapon(string type);
    }
}
