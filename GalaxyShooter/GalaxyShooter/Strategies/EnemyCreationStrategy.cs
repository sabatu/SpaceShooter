﻿using GalaxyShooter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalaxyShooter.Interfaces
{
    public interface EnemyCreationStrategy
    {
        List<Ship> GetRegularEnemy(string type, int SizeOfGroup);
        Ship GetBoss(string type);

    }
}
