Background: a four minute game of a player space-ship set on the backdrop of an alien world. The story can be that the player is an alien defending it's world from invasion. The player starts in a wilderness/desert environment with two different types of low-level enemies attacking in waves. The low-level enemies have slightly different ship designs per type, as well as a different firing pattern depending on the enemy. During this stage, the player gets the opportunity to obtain power-ups and shield bonuses that will be useful for the mid-boss fight. At roughly the 1:00 mark, the mid-boss enters, and the player spends the next minute fighting the mid-boss. The mid-boss is defeated by the 1:30 mark, the player is rewarded with restored HP/shield/power ups. Now all four types of enemies attack the player, with the two 'more difficult' variants using more complicated movement and firing patterns. At roughly the 2:30 - 2:45 mark, the player enters the bombed-out alien city, and the final boss appears at this point. The final boss has three unique stages that the player must destroy in order to defeat the boss.
Level Design: An alien world (that player characters homeworld) where the background starts as desert environment and slowly converges into an bombed-out city.
Initially, have just one screen and have it continuously repeat having it scroll down
Later mix it up with maybe an ocean scene 
(Deliverable 1 requirement):
Enemies: four different types of enemy ships, each with different patterns of weapon attacks and movement algorithms.
Things to Consider for the Class:
Enemy sprite
Enemy health pool
Enemy weapon damage (one shot kills the player or player has a health pool as well)
Color, speed, and number of shots per ship
Enemy path
Number that Spawn
Speed of the enemy
Spawn time
Spawn area
Either at the top of the screen, bottom or somewhere from the sides
Reference below on the imported document that he has provided and what items it is able to take so in consideration of making the classes in the future
�wave�:
	{
	   �id�: �1�,
	   "time": {"0", "1000"},
	   "enemyType": "A",
	   "enemyAmount": "5",
	   "interval": "200",
	   �enemyBulletType":
		{
			"color": "red",
			"type": "B",
			"amount": "20",
			"speed": "10"
		}
	}
(Deliverable 1 requirement):
Mid-boss: special ability to randomly teleport to different sections of the screen, has 'homing missiles' that must be shot down.
Teleporting would be hard and may cause some bugs or crashing, but maybe have it side scroll at the top of the screen and charge down on the left and right edges which can destroy the player


(Deliverable 1 requirement):
Final-boss: A boss with three unique stages (with three different sets of hit points) that must be iteratively worn away in order to defeat the final boss. All of the weapons used by the base enemy types are present on the final boss at one stage or another.
Maybe could spawn previous enemies, but later date idea
Side scroll at the top shooting a lot of bullets in a loop fashion
Stage 1 lvl 1 bullets
Health pool = 50x initial player bullet damage
Stage 2 lvl 2 bullets
Health pool = 75x initial player bullet damage
Stage 3 mix 1 and 2 with multiplier on the number or speed
Health pool = 30x initial player bullet damage
(having it by player bullet damage, maybe can scale when have more than one player playing)
(Deliverable 1 requirement):
Movement speeds: The player shall have three different movement speeds: slow, normal, fast.
Have 2 speed settings with a key that toggles between 0 or 1 for the value of their speed
Player hit-points and shield: Player has default HP and the ability to have a shield if obtained via power-up.
(Requirement for Deliverable 2):
Player lives: The player will start with 3 lives, and get an additional life every 10,000 points they score.
Scoring system: A standard enemy is 100 points. A boss is 5000 points. Additional point bonuses can be earned by killing a certain number of enemies without taking damage.
(These are deliverable 2 'extra credit' worth considering):
Functional Menu System: Player will be allowed to re-assign keys, change difficulty level on-the-fly.
(These are required Deliverable 3 requirements)
Power-ups: different types of weapon modifications  that change firing pattern and rate, bombs picked up from defeating enemies/bosses. Also, if a particular chain of enemies  gets destroyed under a certain amount of time without the player taking any damage, the player gets rewarded for a 'combo attack', and is automatically granted temporary invulnerability/shield/special weapon power-up.
(These are optional Deliverable 3 requirements that are worth thinking about)
Cheat mode:  there will be a console drop-down that will allow for either making the player invulnerable, or giving them weapon/shield powerups on request.
Difficulty settings: this could be as easy as modifying player HP from the beginning, or changing the default damage values that enemies weapons have assigned to them.
Has it change the spawn rate of the player items
changes the speed of the enemy bullets with a multiplier
Multiplies the number of shots the enemy takes
Sound/Music: (I've played a bit with media synthesizers in the past and think it would be pretty do-able to at least have a passable four minute track for the background music..)
Just making a short amount of music or effect, and then looping it during the gameplay
IE metal slug

Future Vision: 
Having a possible player 2 option
This would have the option of coop in the program and having to set an extra set of keys and making an Alpha player ship and a Beta player ship
Overall, if the player class is made right, then it would only have to call it again and modify the keysets
Score based, having different enemies have different values that add to a total score
Keep score at the top right, enemy class has a score value that is modified (this would have the score total built into the game and not the player initially)
Maybe if 2 player initially have the score just be a complete total between the 2 players, and maybe later on we can have it so the different players have different scores 
Have a high score total screen that is saved to a text file that will be imported that contains the high scores and then is overwritten when the game closes
This seems a bit excessive to try to save the names so maybe just the numbers for now and have them quick sorted, or maybe don�t have it save the current high scores like a real arcade machine an imports all the same default scores each time.
IE
12000--AAAAAA
10000--BBBBBB
Having different player ships with different colored weapon bullets (initially without the score implementation maybe just have them free to choose)
Just have to modify the character sprite and weapon color
Some personalization, maybe if the player plays the game a few times, they can unlock maybe some free power-ups or different ships
Unlock with a certain score achieved
Have it in the code so when the player dies or the game ends its cycle have it check the values to certain sprites that have unlocks and have them available
IE - Some steam game im having trouble remembering
Faster Than Light (FTL on steam)
